#!/usr/bin/python3 -O
# coding: utf-8
# author : Simon Descarpentries
# date: 2017-2018
# licence: GPLv3

from doctest import run_docstring_examples
from datetime import datetime
from test_drop_alternatives import test_drop_alternatives

"""
CPU: Intel(R) Xeon(R) CPU           L5420  @ 2.50GHz
OS : Debian Jessie 8.9

2017-10-03 : 11 tests ; 3.34s ; 30.3ms/t 100 runs
2017-10-03 : 11 tests ; 2.24s ; 20.6ms/t (regex del spaces ; compares 200c)
2017-10-03 : 13 tests ; 2.75s ; 21.1ms/t (deps on lxml, compares 100c)
2017-10-04 : 16 tests ; 3.56s ; 22.2ms/t (no deps, no decode, 3 junk-regex, compares 45c)
2017-10-04 : 17 tests ; 3.74s ; 22  ms/t (separating links regexp from junk_txt and _html)
2017-10-05 : 18 tests ; 4.11s ; 22.8ms/t (back to decode, because base64 encoding…)
2017-10-08 : 20 tests ; 4.66s ; 23.3ms/t (html.unescape, one strip regexp, compares 128c)
2017-10-13 : 21 tests ; 6.14s ; 29.2ms/t (compares 90c, from the begining, strip style & title)
2017-10-13 : 21 tests ; 5.44s ; 25.9ms/t (simplify regexp)
2017-10-18 : 22 tests ; 6,80s ; 30.9ms/t (compares 256c, use best ratio function)
2017-10-20 : 22 tests ; 6,70s ; 30.4ms/t (no more .*? nor re.DOTALL in regex)
2017-10-21 : 24 tests ; 6.64s ; 27.6ms/t (strip scripts, no more decode / bytes only)
2017-10-21 : 24 tests ; 5.43s ; 22.6ms/t (run via python2)
2017-10-21 : 25 tests ; 5.72s ; 22.8ms/t (deal with <style><!-- without .*? and re.DOTALL)
2017-10-23 : 26 tests ; 7.53s ; 28.9ms/t (deal with display:none without .*?, compares 199c)
2017-10-25 : 28 tests ; 8.22s ; 29.3ms/t (CSS /* */ proof, compares 256c backward, quick_ratio)
2017-10-26 : 28 tests ; 8.10s ; 28.9ms/t (simplify regexp, .+ -> .*, compares min(len()))
2017-11-10 : 29 tests ; 10.7s ; 36.8ms/t (keep joint files, quick_ratio() on [:256] & [-256:])
2017-11-22 : 30 tests ; 11.9s ; 39.6ms/t (read 40kc of HTML, remove all <*:…></…> which is slow)
2017-12-20 : 30 tests ; 11.2s ; 37.4ms/t (full bytes, display int ratio)

Linux 4.9.0-5-amd64 #1 SMP Debian 4.9.65-3+deb9u2 (2018-01-04) x86_64 GNU/Linux
Debian Stretch 9.3
Python 2.7.13

2018-01-11 : 30 tests ; 11.7s ; 39.1ms/t (major system update, lost perf…)
2018-03-14 : 32 tests ; 13.1s ; 40.9ms/t (remove HTML comments ; LEN=280 ; LIM=.82 ; BON=.91)
2018-03-15 : 32 tests ; 1.42s ; 44.3ms/t ( / 10 instead of / 100 in measurements)
2018-03-19 : 33 tests ; 1.50s ; 45.4ms/t (clean more ASCII art, 'cause EFF… ; slower real tests)
2018-03-20 : 34 tests ; 1.55s ; 45.5ms/t (non-breakable space is chunk delimiter, \v\f=bad c.)
2018-03-24 : 34 tests ; 1.53s ; 45.0ms/t (re-order HTML tag name of leaf to strip and bad chars)

Python 3.5.3

2018-04-11 : 34 tests ; 1.65s ; 48.5ms/t (Python 3.5.3 : ~8% slower)
2018-04-12 : 31 tests ; 0.93s ; 30.0ms/t (major rewrite, act on message/alternative, -3fake tst)
2018-04-13 : 31 tests ; 0.93s ; 30.0ms/t ('in' -> '==' ; words -> trigrams ; recur -> iter :-( )
2018-04-14 : 31 tests ; 0.93s ; 30.0ms/t ('==' -> 'startswith' :-( )
2018-04-16 : 34 tests ; 1.72s ; 50.5ms/t (re-insert comparison stuff)
2018-04-19 : 36 tests ; 2.78s ; 77.2ms/t (real testcases are slower, line_profile analysis)
2018-04-24 : 37 tests ; 2.79s ; 75.4ms/t (new hyp test on skip treatment trigger)
2018-04-25 : 37 tests ; 2.80s ; 75.6ms/t (in 50%< startswith, append/pop 30%< inser(0,_)/pop(0))
2018-05-16 : 37 tests ; 2.80s ; 75.6ms/t (.lower() each compared text portions)
2018-06-20 : 38 tests ; 1.95s ; 51.3ms/t (don't compare with zero-sized elts, 40k HTML read)
2018-07-08 : 39 tests ; 1.97s ; 50.5ms/t (startswith avoid mistakes : message <> officedocument)

Debian 11.2
Python 3.9.2

2022-01-07 : 39 tests ; 1.62s ; 41.5ms/t (upgrade to Python 3.9 : ~20% faster)

Linux tabr 6.1.0-18-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.76-1 (2024-02-01) x86_64 GNU/Linux
Debian 12.5
Python 3.11.2

2024-04-16 : 39 tests ; 1.40s ; 35.9ms/t (upgrade to Python 3.11 : ~15% faster)
2024-04-18 : 39 tests ; 1.53s ; 39.2ms/t (keep sub_part embeded application/pdf and images/)

"""

startTime = datetime.now()

for i in range(0, 10):
	run_docstring_examples(test_drop_alternatives, globals())

print(datetime.now() - startTime)
