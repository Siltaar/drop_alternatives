#!/usr/bin/python3 -O
# coding: utf-8
# author : Simon Descarpentries - simon /\ acoeuro [] com
# date: 2017 - 2018
# licence: GPLv3

from email.parser import BytesParser
from email.mime.multipart import MIMEMultipart
from email.iterators import _structure	# noqa
from sys import stdin, stderr, argv
from difflib import SequenceMatcher
from re import DOTALL, compile as compile_re
from html import unescape


purge_html_re = compile_re(  # match, to remove :
	b'<(sty|(o|w):|scr|tit|[^y]*y\s*:\s*(n|h)).*?</[^>]*'
	# style, o: w:, script, title, display:none / hidden HTML tags, text leaf, partial ending
	# '<(sty|(o|w):|scr|tit|[^y]*yle="display\s*:\s*(n|h)).*?</[^>]*'
	b'|<!--.*?-->'  # HTML comments
	b'|<[^>]*'		# all complete HTML tags,  # some may be cut at the end/begining
	b'|&[^;]*;'		# HTML entities
	b'|[\d*]'		# links prefix in converted texts
	b'|[^\s<\xc2\xa0]{25,}',  # - chunks of symbols without spaces too big to be words (as URL)
	DOTALL)
bad_chars = b' >\n\xc2\xa0.,@#-=:*][+_()/|\'\t\r\f\v\\'
W = '\033[0m'  # white (normal)
G = '\033[1;30m'  # grey
R = '\033[1;31m'  # bold red
Y = '\033[1;33m'  # bold yellow
B = '\033[1;37m'  # bold white
LEN = 280
LIM = .82
BON = .91


def drop_alternatives(msg_bytes):
	eml = BytesParser().parsebytes(msg_bytes)
	# __debug__ and _structure(eml)

	if not eml.is_multipart():
		__debug__ and print('not multipart', file=stderr)
		return eml

	if not len([part for part in eml.walk() if part.get_content_subtype().startswith('htm')]):
		__debug__ and print('no HTML to drop', file=stderr)
		return eml

	flat_eml = [[part for part in eml.walk()]]
	flat_eml[0].reverse()
	new_eml = [clone_message(eml)]
	x_drop_alt = []

	if not eml.get_content_subtype().startswith('alt'):  # alternative
		flat_eml[0].pop()  # replaced by multipart/mixed

	while len(flat_eml) > 0:
		while len(flat_eml[-1]) > 0:
			part = flat_eml[-1].pop()
			__debug__ and print(part.get_content_type(), end=' ', file=stderr)
			part_content_subtype = part.get_content_subtype()

			if part_content_subtype.startswith('alt') and len(flat_eml[-1]) > 1:  # alternative
				candidate_txt = flat_eml[-1].pop()
				new_eml[-1].attach(candidate_txt)
				__debug__ and print('txt '+candidate_txt.get_content_type(), end=' ', file=stderr)

				candidate_htm = flat_eml[-1].pop()
				__debug__ and print('htm '+candidate_htm.get_content_type(), end=' ', file=stderr)
				candidate_htm_subtype = candidate_htm.get_content_subtype()

				if candidate_htm_subtype.startswith('htm'):  # html
					if are_idem_txt(candidate_txt, candidate_htm):
						x_drop_alt.append(candidate_htm_subtype)  # drop the copy
					else:
						new_eml[-1].attach(candidate_htm)
						__debug__ and print('htm diff', end=' ', file=stderr)
				elif candidate_htm_subtype.startswith('rel'):  # related
					sub_part = flat_eml[-1].pop()

					while not sub_part.is_multipart() and len(flat_eml[-1]) > 0:
						sub_part_ctype = sub_part.get_content_subtype()
						if sub_part_ctype.startswith('htm'):
							if are_idem_txt(candidate_txt, sub_part):
								x_drop_alt.append(sub_part_ctype)
							else:
								new_eml[-1].attach(sub_part)
								__debug__ and print('rel htm diff', end=' ', file=stderr)
						else:
							# application/pdf /…
							if not sub_part_ctype.startswith('app') or \
								not sub_part_ctype.startswith('image'):  # image/png /…
								x_drop_alt.append(sub_part.get_content_type())
							else:
								new_eml[-1].attach(sub_part)
								__debug__ and print('	 kept', file=stderr)

						sub_part = flat_eml[-1].pop()  # consume intput

					if sub_part.is_multipart():
						flat_eml[-1].append(sub_part)
				else:  # unknown configuration yet
					new_eml[-1].attach(candidate_htm)
					__debug__ and print('new configuration ?', end=' ', file=stderr)
			elif part.get_content_type().startswith('me'):  # message
				__debug__ and print(' clne', file=stderr)
				flat_sub_eml = [sub_part for sub_part in part.walk()]
				flat_sub_eml.reverse()
				flat_sub_eml.pop()  # replaced by multipart/mixed
				flat_eml[-1] = flat_eml[-1][:-len(flat_sub_eml)]  # consume input
				flat_eml.append(flat_sub_eml)
				new_eml.append(clone_message(part))
			elif part_content_subtype.startswith('rel'):  # related
				__debug__ and print('drop rel', file=stderr)
			else:
				new_eml[-1].attach(part)
				__debug__ and print('	 kept', file=stderr)

		if len(new_eml) > 1:
			new_eml[-2].attach(new_eml[-1])
			new_eml.pop()

		flat_eml.pop()

	if len(x_drop_alt):
		new_eml[0]['x-drop-alt'] = ', '.join(x_drop_alt)
		# __debug__ and _structure(new_eml[0])
		return new_eml[0]
	else:
		return eml


def are_idem_txt(part_txt, part_htm):
	txt_1, txt_2 = get_txt(part_txt, 3000)
	htm_1, htm_2 = get_txt(part_htm, 40000)
	s_1, s_2 = min(len(htm_1), len(txt_1)), min(len(htm_2), len(txt_2))

	if s_1 == 0 or s_2 == 0:
		return False

	idem_ratio_deb = SequenceMatcher(a=htm_1[:s_1], b=txt_1[:s_1]).quick_ratio()
	idem_ratio_fin = SequenceMatcher(a=htm_2[-s_2:], b=txt_2[-s_2:]).quick_ratio()
	idem_ratio = (idem_ratio_deb + idem_ratio_fin) / 2

	if __debug__:
		from shutil import get_terminal_size
		ir = ' '+color_ratio(idem_ratio)
		cols, rows = get_terminal_size()
		PUT_txt = int(cols) - 10
		PUT_htm = int(cols) - 9

		def put(string, postfix, size):
			print(string[:size].ljust(size, '.') + postfix, file=stderr)

		if idem_ratio_deb < LIM or True:
			put('\n' + str(htm_1, errors='replace'), W + ' <H', PUT_htm - 1)
			put(str(txt_1, errors='replace'), ' T '+color_ratio(idem_ratio_deb)+ir, PUT_txt)
		if idem_ratio_fin < LIM:  # or True:
			put('\n' + str(htm_2, errors='replace'), W + ' H>', PUT_htm)
			put(str(txt_2, errors='replace'), ' T '+color_ratio(idem_ratio_fin)+ir, PUT_txt)

	return idem_ratio_deb > BON or idem_ratio_fin > BON or idem_ratio > LIM


def get_txt(part, raw_len, bad_chars=bad_chars):
	t = part.get_payload(decode=True)
	if not t:
		return 'a', 'b'  # return two different strings to keep the part
	t_start = t[:raw_len]
	t_end = t[-raw_len:]
	t_start = purge_html_re.sub(b'', t_start)
	t_start = t_start.translate(None, bad_chars)
	t_end = purge_html_re.sub(b'', t_end)
	t_end = t_end.translate(None, bad_chars)
	# Python 2 ratio() changes its behavior after 199c
	return t_start[:LEN].lower(), t_end[-LEN:].lower()


def color_ratio(ratio):
	C = ratio < LIM and R or ratio < BON and Y or W
	return str(C + str(int(ratio*100)) + W)


def clone_message(eml):
	new_eml = MIMEMultipart(_subtype=eml.get_content_subtype(), boundary=eml.get_boundary())

	for k, v in eml.items():  # `eml` have only headers as its items
		if k not in ["content-length", "lines", "status"]:  # unwanted fields
			# Python will set its own Content-Type and Content-Length lines in any case
			# Lines may change anyway
			# What is Status ?
			new_eml[k] = v

	new_eml.preamble = eml.preamble
	new_eml.epilogue = eml.epilogue

	if len(eml.defects):
		new_eml['x-python-parsing-defects'] = str(eml.defects)

	return new_eml


if __name__ == "__main__":
	debug = 0

	for i, arg in enumerate(argv):
		if i == 0:  # 1st arg of argv is the program name
			continue
		elif '--debug' in arg:
			debug = 1

	if debug:
		drop_alternatives(stdin.buffer.raw.read())
	else:
		input_eml = stdin.buffer.raw.read()  # separate lines to allow debugging
		output_eml = drop_alternatives(input_eml)
		str_eml = str(output_eml.as_bytes(), errors='replace')
		print(unescape(str_eml), end='')
